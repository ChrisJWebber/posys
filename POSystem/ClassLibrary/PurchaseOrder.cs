﻿using System.Collections.Generic;
using System.Linq;

namespace ClassLibrary
{
    public class PurchaseOrder
    {
        private string orderNumber;
        private List<OrderLine> orderLines = new List<OrderLine>();

        public string OrderNumber { get { return orderNumber; } }
        public decimal TotalPrice { get { return orderLines.Sum(x => x.TotalPrice) + DeliveryCharge; } }
        public decimal DeliveryCharge { get; set; }
        public List<OrderLine> OrderLines { get { return orderLines; } }

        public PurchaseOrder ()
        {
            orderNumber = DataAccess.GeneratePOID();
        }

        public void AddOrderLines (List<OrderLine> lines)
        {
            orderLines.AddRange(lines);
        }

    }
}
