﻿
namespace ClassLibrary
{
    public class OrderLine
    {
        public string ProductID { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public decimal PricePerUnitExVAT { get; set; }
        public double PercentageDiscount { get; set; }
        public decimal TotalPrice 
        { get
            {
                return (decimal)(Quantity * (1 + (Constants.PercentageVAT/100)) * (1 - (PercentageDiscount/100))) * PricePerUnitExVAT;
            }
        }
    }
}
