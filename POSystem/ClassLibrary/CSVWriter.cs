﻿using System.IO;

namespace ClassLibrary
{
    public class CSVWriter
    {
        public string DestinationPath { get; set; }
 
        public CSVWriter (string destination)
        {
            DestinationPath = destination;
        }

        public void WritePOToCSV (PurchaseOrder PO)
        {
            try
            {
                using (StreamWriter streamWriter = new StreamWriter(DestinationPath, false))
                {                    
                    //Write csv headers
                    streamWriter.WriteLine("Headers");

                    //Write order lines
                    foreach (OrderLine line in PO.OrderLines)
                    {
                        streamWriter.WriteLine($"{line.ProductID},{line.Description},{line.Quantity},{line.PricePerUnitExVAT},{line.PercentageDiscount}");
                    }

                    streamWriter.WriteLine();
                    streamWriter.WriteLine($"Delivery,{PO.DeliveryCharge}");

                    streamWriter.Close();
                }
            }
            catch (System.Exception ex)
            {
                throw ex;                
            }
        }

    }
}
