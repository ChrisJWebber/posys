﻿using System.Collections.Generic;

namespace ClassLibrary
{
    public static class DataAccess
    {
        static int id = 1;
        static string user = "CW";

        public static List<OrderLine> LoadSampleData ()
        {
            List<OrderLine> lines = new List<OrderLine>
            {
                new OrderLine {ProductID = "454-5787", Description = "Ferrules", Quantity = 200, PricePerUnitExVAT = 0.55M, PercentageDiscount = 0 },
                new OrderLine {ProductID = "Xd12", Description = "PSU", Quantity = 1, PricePerUnitExVAT = 250.00M, PercentageDiscount = 0 },
                new OrderLine {ProductID = "PNOZS5", Description = "SafetyRelay", Quantity = 4, PricePerUnitExVAT = 75.00M, PercentageDiscount = 0 },
            };

            return lines;
        }

        internal static string GeneratePOID()
        {
            return $"{id++:#,##000}{user}";
        }
    }
}
