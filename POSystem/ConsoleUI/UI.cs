﻿using System;
using System.Dynamic;

namespace ConsoleUI
{
    class UI
    {
        public static void ClearScreen()
        {
            Console.Clear();
        }
        public static void DisplayGreetingMessage()
        {
            Console.WriteLine("Welcome to the PO System");
        }

        public static void DisplayEndMessage()
        {
            Console.WriteLine("Press Enter to exit the application: ");
            Console.ReadLine();
        }

        public static void DisplayMessage(string message)
        {
            Console.WriteLine(message);
        }

        public static void DisplayFileWritingMessage()
        {
            Console.WriteLine("Writing PO to file");
        }

        public static void DisplayEndFileWritingMessage()
        {
            Console.WriteLine("Writing complete");
        }

        public static void DisplayDeliveryCharge (decimal charge)
        {
            Console.WriteLine($"Delivery: {charge}");
        }

        public static void DisplayTotalPrice (string orderNumber, decimal price)
        {
            Console.WriteLine();
            Console.WriteLine($"Total price of order {orderNumber} = {price:C2}");
            Console.WriteLine();
        }

        public static bool AskForMoreOrders()
        {
            string response = "";
            bool responseValid = false;
            do
            {
                Console.WriteLine("Are there any more items to order? y/n: ");
                response = Console.ReadLine().ToLower();
                if (response == "y")
                {
                    return true;
                }
                else if (response == "n")
                {
                    responseValid = true;
                }
            } while (responseValid == false);

            return false;
        }
        public static string AskForProductID()
        {
                return GetNonBlankString("Enter the product ID: ");
        }

        public static string AskForProductDescription()
        {
            return GetNonBlankString("Enter the product description: ");
        }
        public static int AskForQuantity()
        {
            int output = 0;
            do
            {
                Console.Write("Enter the quantity: ");

                try
                {
                    output = Int32.Parse(Console.ReadLine()); 
                    if (output <1)
                    {
                        output = 0;
                        throw new Exception();                        
                    }
                }
                catch 
                {
                    Console.WriteLine("The quantity must be a positive integer");
                }
            } while (output == 0);

            return output;
        }
        public static decimal AskForPricePerUnit()
        {
            return GetNonNegativeDecimal("Enter the per unit price excluding VAT: ");
        }
        public static double AskForPercentageDiscount()
        {
            double output = -1;
            do
            {
                Console.Write("Enter the percentage discount: ");

                try
                {
                    output = double.Parse(Console.ReadLine().Replace("%",""));

                    if (output < 0 || output > 100)
                    {
                        Console.WriteLine("The discount must be between 0 and 100%");
                        output = -1;
                    }
                }
                catch 
                {
                    Console.WriteLine("Invalid input");
                }
            } while (output == -1);


            return output;
        }
        public static decimal AskForDeliveryCharge()
        {
            return GetNonNegativeDecimal("Enter the delivery charge: ");
        }

        public static void DisplayOrderLine(string ID, string description, int quantity, decimal perUnitCost, double discount )
        {
            Console.WriteLine($"ID: {ID}, Description: {description}, " +
                                    $"Quantity: {quantity}, Price per Unit: {perUnitCost:C2} " +
                                    $"Percentage discount: {discount}%");
        }

        private static string GetNonBlankString(string prompt)
        {
            string output = "";
            do
            {
                Console.Write(prompt);
                output = Console.ReadLine();

                if (output == "")
                {
                    Console.WriteLine("Entry cannot be blank");
                }
            } while (output == "");

            return output;
        }

        private static decimal GetNonNegativeDecimal (string prompt)
        {
            decimal output = 0;
            do
            {
                Console.Write(prompt);

                try
                {
                    output = decimal.Parse(Console.ReadLine().Replace("£", ""));
                    if (output < 0)
                    {
                        output = 0;
                        Console.WriteLine("Value cannot be negative");
                    }
                    else
                    {
                        return output;
                    }
                }
                catch
                {
                    Console.WriteLine("Invalid input");
                }
            } while (output == 0);

            return output;
        }

    }
}
