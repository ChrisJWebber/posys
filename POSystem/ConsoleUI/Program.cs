﻿using ClassLibrary;
using System.Collections.Generic;

namespace ConsoleUI
{
    class Program
    {

        static void Main(string[] args)
        {

            UI.DisplayGreetingMessage();

            PurchaseOrder PO = new PurchaseOrder();

            //Populate PO from User input
            PO.AddOrderLines(BuildPO());
            PO.DeliveryCharge = UI.AskForDeliveryCharge();

            //Display PO lines
            UI.ClearScreen();
            foreach (OrderLine lines in PO.OrderLines)
            {
                UI.DisplayOrderLine( lines.ProductID,
                                     lines.Description,
                                     lines.Quantity,
                                     lines.PricePerUnitExVAT,
                                     lines.PercentageDiscount);
            }

            UI.DisplayDeliveryCharge(PO.DeliveryCharge);
            UI.DisplayTotalPrice(PO.OrderNumber, PO.TotalPrice);

            //Write PO to file
            try
            {
                UI.DisplayFileWritingMessage();
                CSVWriter CSVWriter = new CSVWriter($"C:\\Work Area\\Practice\\POsys\\CSVs\\{PO.OrderNumber}.csv");
                CSVWriter.WritePOToCSV(PO);
                UI.DisplayEndFileWritingMessage();
            }
            catch (System.Exception ex)
            {
                UI.DisplayMessage(ex.Message);
                
            }

            UI.DisplayEndMessage();

        }

        static List<OrderLine> BuildPO()
        {
            List<OrderLine> output = new List<OrderLine>();
            while (UI.AskForMoreOrders() == true)
            {
                output.Add(
                    new OrderLine
                    {
                        ProductID           = UI.AskForProductID(),
                        Description         = UI.AskForProductDescription(),
                        Quantity            = UI.AskForQuantity(),
                        PricePerUnitExVAT   = UI.AskForPricePerUnit(),
                        PercentageDiscount  = UI.AskForPercentageDiscount()
                    }
                );                 
            }
            return output;
        }

    }
}
